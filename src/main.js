import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import {store} from './store/store'

//Using the Vue Router 
Vue.use(VueRouter);

//Using the Vue Resource
Vue.use(VueResource);



import {routes} from './routes';

export const router = new VueRouter({

	routes,
	mode:'history',

	scrollBehavior (to,from,savedPosition) {

		if(savedPosition){

			return savedPosition;
		}

		else {

			return {x:0,y:0};

		}

		

	}

});



// Importing Shared Components

// import Navbar from './components/shared/navbar.vue'
import Navbar from './components/shared/navbar2.vue';
import Footer from './components/shared/footer.vue';

// Adding the Shared Components
 Vue.component('app-navbar', Navbar);
 Vue.component('app-footer',Footer);





// New Documentation Shared Components
// import Navbar from './components/shared/navbar2.vue';
// import Footer from './components/shared/footer.vue';
// import Comments from './components/Share/Comments.vue';

// Adding the Shared Components
// Vue.component('app-navbar', Navbar);
// Vue.component('app-footer',Footer);
// Vue.component('app-comments',Comments);







new Vue({

  el: '#app',
  router,
  store,
  render: h => h(App)

})
