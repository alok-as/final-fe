// import Home from './components/Pages/homepage.vue';

import Home from './components/Home/HomePage.vue';


const About = resolve=>{

	require.ensure(['./components/SinglePages/Aboutus.vue'],()=> {

		resolve(require('./components/SinglePages/Aboutus.vue'));
	
	});
}

const Blog = resolve=>{

	require.ensure(['./components/Blog/Listings.vue'],()=> {

		resolve(require('./components/Blog/Listings.vue'));
	
	});
}

const Faq = resolve=>{

	require.ensure(['./components/SinglePages/Faq.vue'],()=> {

		resolve(require('./components/SinglePages/Faq.vue'));
	
	});
}

const Featured = resolve=>{

	require.ensure(['./components/Featured/Featured.vue'],()=> {

		resolve(require('./components/Featured/Featured.vue'));
	
	});
}

const Hotel = resolve=>{

	require.ensure(['./components/Featured/FeaturedLeelaWati.vue'],()=> {

		resolve(require('./components/Featured/FeaturedLeelaWati.vue'));
	
	});
}

const Mainblog = resolve=>{

	require.ensure(['./components/Blog/ListingsSingleBlog.vue'],()=> {

		resolve(require('./components/Blog/ListingsSingleBlog.vue'));
	
	});
}

const Prizing = resolve=>{

	require.ensure(['./components/SinglePages/Prizing.vue'],()=> {

		resolve(require('./components/SinglePages/Prizing.vue'));
	
	});
}

const Working = resolve=>{

	require.ensure(['./components/SinglePages/Working.vue'],()=> {

		resolve(require('./components/SinglePages/Working.vue'));
	
	});
}


const Result = resolve=>{

	require.ensure(['./components/Result/Result.vue'],()=> {

		resolve(require('./components/Result/Result.vue'));
	
	});
}

const Learn = resolve=>{

	require.ensure(['./components/SinglePages/Learn.vue'],()=> {

		resolve(require('./components/SinglePages/Learn.vue'));
	
	});
}


// const About = resolve=>{

// 	require.ensure(['./components/Pages/Aboutus.vue'],()=> {

// 		resolve(require('./components/Pages/Aboutus.vue'));
	
// 	});
// }

// const Blog = resolve=>{

// 	require.ensure(['./components/Pages/Bloglist.vue'],()=> {

// 		resolve(require('./components/Pages/Bloglist.vue'));
	
// 	});
// }

// const Faq = resolve=>{

// 	require.ensure(['./components/Pages/Faq.vue'],()=> {

// 		resolve(require('./components/Pages/Faq.vue'));
	
// 	});
// }

// const Featured = resolve=>{

// 	require.ensure(['./components/Pages/Featured.vue'],()=> {

// 		resolve(require('./components/Pages/Featured.vue'));
	
// 	});
// }

// const Hotel = resolve=>{

// 	require.ensure(['./components/Pages/hotel.vue'],()=> {

// 		resolve(require('./components/Pages/hotel.vue'));
	
// 	});
// }

// const Mainblog = resolve=>{

// 	require.ensure(['./components/Pages/mainblog.vue'],()=> {

// 		resolve(require('./components/Pages/mainblog.vue'));
	
// 	});
// }

// const Prizing = resolve=>{

// 	require.ensure(['./components/Pages/Prizing.vue'],()=> {

// 		resolve(require('./components/Pages/Prizing.vue'));
	
// 	});
// }

// const Working = resolve=>{

// 	require.ensure(['./components/Pages/Working.vue'],()=> {

// 		resolve(require('./components/Pages/Working.vue'));
	
// 	});
// }


// const Result = resolve=>{

// 	require.ensure(['./components/Pages/Result.vue'],()=> {

// 		resolve(require('./components/Pages/Result.vue'));
	
// 	});
// }

// const Learn = resolve=>{

// 	require.ensure(['./components/Pages/learn.vue'],()=> {

// 		resolve(require('./components/Pages/learn.vue'));
	
// 	});
// }




export const routes= [

	{ path:'',component:Home},
	{path:'/about',component:About},
	{path:'/blog',component:Blog},
	{path:'/faq',component:Faq},
	{path:'/featured',component:Featured},
	{path:'/hotel',component:Hotel},
	{path:'/mainblog',component:Mainblog},
	{path:'/prizing',component:Prizing},
	{path:'/working',component:Working},
	{path:'/result',component:Result},
	{path:'/learn',component:Learn},
	{path:'*',redirect:'/'}
	
	

];




