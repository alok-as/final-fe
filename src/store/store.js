import	Vue from 'vue';
import Vuex from 'vuex';
import VueResource from 'vue-resource'
import {router} from '../main.js'

Vue.use(Vuex);
Vue.use(VueResource);

export const store = new Vuex.Store({

	state:{


		// for receiving the response
		result:[],

		// for Taking the user input
		category:'',
		location:''




	},

	getters: {

		cValue:state=> {

			return state.category;

		},

		lValue:state=> {

			return state.location;
		}



	},

	mutations: {

		// Methods for Initialzing the category and location values

		initcValue:(state,payload)=> {

			state.category=payload;


		},

		initlValue:(state,payload)=> {

			state.location=payload;
			
		},



	},

	actions: {

		initcValue:({commit},payload)=> {

			commit('initcValue',payload);

		},

		initlValue:({commit},payload)=> {

			commit('initlValue',payload);

		},


		// Making the http request for getting results
		
		getResult:({commit,state})=> {

			Vue.http.get("https://e5059192.ngrok.io/getGoogleData",{

				category:state.category,
				cities:state.location

			}).then(response=>{

				// console.log(response);

				return response.json();

			}).then(data=>{

				console.log(data);

				const Resultarray=[];

				for(let key in data) {

					Resultarray.push(data[key]);
				}

				store.result=Resultarray;
			
			});


			router.push({

				path:'/result',
				query:{ searchCategory:state.category,searchLocation:state.location} 

			});

		}



	}



})